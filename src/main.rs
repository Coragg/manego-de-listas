mod input_user;
mod manege_vec;
mod read_file;
use input_user::input_string;
use manege_vec::{insercciones, list_to_analizy};
use read_file::read_file;
use std::time::{Duration, Instant};

fn interface() -> () {
    let first_file: String = input_string("Ingrese el nombre del primer archivo: ");
    let second_file: String = input_string("Ingrese el nombre del segundo archivo: ");
    let start_time_get_matrix: Instant = Instant::now();
    let get_first_list_of_the_file: Vec<i32> = read_file(first_file);
    let get_second_list_of_the_file: Vec<i32> = read_file(second_file);
    let end_time_get_matrix: Duration = start_time_get_matrix.elapsed();
    let start_time_analizy: Instant = Instant::now();
    list_to_analizy("PPRIMERA LISTA", &get_first_list_of_the_file);
    list_to_analizy("SEGUNDA LISTA", &get_second_list_of_the_file);
    let end_time_analizy: Duration = start_time_analizy.elapsed();
    let start_time_inserccioens: Instant = Instant::now();
    insercciones(
        get_first_list_of_the_file.clone(),
        get_second_list_of_the_file.clone(),
    );
    let end_time_insercciones: Duration = start_time_inserccioens.elapsed();
    println!("\nCalcular tiempo: \nTiempo en la lectura de archivo: {} \nTiempo en analisis: {} \nTiempo en inserccion: {}", end_time_get_matrix.as_micros(), end_time_analizy.as_micros(), end_time_insercciones.as_micros())
}

fn main() {
    interface();
}
