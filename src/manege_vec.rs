fn minor_element(list_of_numbers: &Vec<i32>) -> i32 {
    let mut less_number: i32 = list_of_numbers[0];
    let mut index: usize = 0;

    while index < list_of_numbers.len() {
        if less_number > list_of_numbers[index] {
            less_number = list_of_numbers[index];
        }
        index += 1;
    }
    less_number
}


fn major_element(list_of_numbers: &Vec<i32>) -> i32 {
    let mut major_number: i32 = list_of_numbers[0];
    let mut index: usize = 0;

    while index < list_of_numbers.len() {
        if major_number < list_of_numbers[index] {
            major_number = list_of_numbers[index];
        }
        index += 1;
    }
    major_number
}


fn sumatory(list_of_numbers: &Vec<i32>) -> i32 {
    let mut sum: i32 = 0;
    for number in list_of_numbers.iter(){
        sum += number;
    }
    sum
}


fn show_list_as_string(list_vec: &Vec<i32>) -> String {
    let mut index: usize = 0;
    let mut get_numers: String = String::new();
    while index < list_vec.len() {
        get_numers.push_str(&format!(" {}", list_vec[index].to_string()));
        index += 1;
    }
    get_numers
}


fn backwards_list(list_of_numbers: &Vec<i32>) -> String {
    let mut index: usize = list_of_numbers.len();
    let mut get_numbers: String = String::new();
    while index > 0 {
        index -= 1;
        get_numbers.push_str(&format!(" {}", list_of_numbers[index].to_string()));
    }
    get_numbers
}


fn bubble_sort(list_of_number: &Vec<i32>) -> Vec<i32> {
    let mut swapped: bool = true;
    let mut arr: Vec<i32> = list_of_number.clone();
    while swapped {
        swapped = false;
        for i in 0..arr.len()-1 {
            if arr[i] > arr[i + 1] {
                arr.swap(i, i + 1);
                swapped = true;
            }
        }
    }
    arr
}


pub fn list_to_analizy(name_list: &str, get_list: &Vec<i32>) -> () {
    println!("\n{}:{}", name_list, show_list_as_string(&get_list));
    println!("Largo: {}", &get_list.len());
    println!("Menor elemento: {}", minor_element(&get_list));
    println!("Mayor elemento: {}", major_element(&get_list));
    println!("Suma de los elementos: {}", sumatory(&get_list));
    println!("Lista invertida:{}", backwards_list(&get_list));
    println!("Lista ordenada:{}", show_list_as_string(&bubble_sort(&get_list)))
}


fn join_list(first: Vec<i32>, second_list: Vec<i32>) -> Vec<i32> {
    let mut join_table: Vec<i32> = Vec::new();
    for number_firt_list in first {
        join_table.push(number_firt_list);
    }
    for number in second_list {
        join_table.push(number);
    }
    join_table
}


fn inserction_list(first: Vec<i32>, second_list: Vec<i32>) -> Vec<i32> {
    let mut join: Vec<i32> = Vec::new();
    for number_first_list in first.iter() {
        for number_second_list in second_list.iter() {
            if number_first_list == number_second_list {
                join.push(*number_first_list)
            }
        }
    }
    join
} 


fn resta_list(first_list: Vec<i32>, second_list: Vec<i32>) -> Vec<i32> {
    let mut resta: Vec<i32> = first_list.clone();
    let mut index: usize = 0;
    while  index < resta.len() {
        let mut second_index: usize = 0;
        let mut not_remove: u8 = 0;
        while second_index < second_list.len() {
            if resta[index] == second_list[second_index] {
                resta.remove(index);
                not_remove = 1;
                break;
            }
            second_index += 1;
        }
        if not_remove == 0 {
            index += 1;
        } else {
            index = index;
        }
    } 
    resta
}


pub fn insercciones(first_list: Vec<i32>, second_list: Vec<i32>) -> () {
    let join_table: Vec<i32> = join_list(first_list.clone(), second_list.clone());
    let intersection_table: Vec<i32> = inserction_list(first_list.clone(), second_list.clone());
    let substract_table: Vec<i32> = resta_list(first_list.clone(), second_list.clone());
    println!("\nUNION: {}", show_list_as_string(&join_table));
    println!("UNION ORDENADA: {}", show_list_as_string(&bubble_sort(&join_table)));
    println!("INTERSECCION:{}", show_list_as_string(&intersection_table));
    println!("INTERSECCION ORDENADA:{}", show_list_as_string(&bubble_sort(&intersection_table)));
    println!("RESTA:{}", show_list_as_string(&substract_table));
    println!("RESTA ORDENADA:{}", show_list_as_string(&bubble_sort(&substract_table)))
}
