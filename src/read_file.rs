use std::fs::File;
use std::io::{BufRead, BufReader};


pub fn read_file(name_file: String) -> Vec<i32> {
    let directory_path: String = format!("data/{}", name_file);     
    let file: File = File::open(directory_path).expect("Error to open the file");
    let reader: BufReader<File> = BufReader::new(file);
    reader.lines()
    .map(|line| line.expect("Error of read").parse::<i32>().expect("Error of converstion"))
    .collect()
}

