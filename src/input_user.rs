use std::io::{self, Write};


pub fn input_string(message: &str) -> String {
    print!("{}", message);
    io::stdout().flush().unwrap();
    let mut get_string: String = String::new(); 
    io::stdin().read_line(&mut get_string).expect("Failed to read line");
    let variable: String = get_string.trim().to_string();
    variable
}
